#GNU Makefile
vpath %.cc src
vpath %.hh src
OBJDIR  := obj

CXXFLAGS := -Wall --std=c++20

TARGET   := matveclib
TARGET_T := $(addsuffix _test, $(TARGET))
MAIN_CC  := main.cc
SRCS     := $(MAIN_CC) vector.cc quaternions.cc matrix.cc
SRCS_T   := $(filter-out $(MAIN_CC), $(SRCS)) tests.cc catch.cc tests_matrix.cc quaternion_tests.cc #catch_vec_generator.cc
OBJS     := $(SRCS:.cc=.o)
OBJS_T   := $(SRCS_T:.cc=.o)

#ifeq ($(MAKECMDGOALS), $(TARGET))
#CXXFLAGS += -O2
#endif
ifeq ($(MAKECMDGOALS), run)
CXXFLAGS += -O2
else
CXXFLAGS += -O0 -g
endif

.PHONY: test
test : $(TARGET_T)
	./$(TARGET_T)

.PHONY: run
run : $(TARGET)
	./$(TARGET)

.PHONY: debug
debug : $(TARGET)
	./$(TARGET)

$(TARGET) : $(SRCS) $(addprefix $(OBJDIR)/,$(OBJS))
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o $(TARGET) $(addprefix $(OBJDIR)/,$(OBJS))

$(TARGET_T) : $(SRCS_T) $(addprefix $(OBJDIR)/,$(OBJS_T))
	$(CXX) $(CXXFLAGS) -o $(TARGET_T) $(addprefix $(OBJDIR)/,$(OBJS_T))

$(OBJDIR)/main.o   : vector.hh quaternions.hh
$(OBJDIR)/tests.o  : vector.hh
$(OBJDIR)/vector.o : vector.hh
$(OBJDIR)/matrix.o : matrix.hh
$(OBJDIR)/tests_matrix.o  : vector.hh matrix.hh
$(OBJDIR)/quaternions.o  : quaternions.hh
$(OBJDIR)/quaternion_tests.o  : quaternions.hh

$(OBJDIR)/tests.o : tests.cc | $(OBJDIR)
	sed "{s/scenario/SCENARIO/g; s/given/GIVEN/g; s/when/WHEN/g; \
	     s/then/THEN/g; s/check/CHECK/g; s/require/REQUIRE/g}" $< >tests_tmp
	mv tests_tmp $<
	$(CXX) -c $(CXXFLAGS) -o $@ $<

$(OBJDIR)/%.o : %.cc | $(OBJDIR)
	$(CXX) -c $(CXXFLAGS) -o $@ $<

$(OBJDIR) :
	mkdir -p $(OBJDIR)

.PHONY: clean
clean :
	rm -rf $(OBJDIR)
	rm -f $(TARGET) $(TARGET_T)
