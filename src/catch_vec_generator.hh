#ifndef CATCH_VEC_GEN_HH
#define CATCH_VEC_GEN_HH
#include <random>

#include "../include/catch.hpp"
#include "vector.hh"

class RandomVector2Generator : public Catch::Generators::IGenerator<vec2d<float>> {
    std::minstd_rand m_rand;
    std::uniform_real_distribution<> m_dist;
    float current_number;
	vec2d<float> current_vector;
public:

    RandomVector2Generator(float low, float high):
        m_rand(),
        m_dist(low, high)
    {
        static_cast<void>(next());
    }

	//vec2d<float> const& get() const override;
	vec2d<float> const& get() const;
		//	return current_vector;
		//}
	bool next() override;
		//    current_number = m_dist(m_rand);
		//    current_vector = {current_number, (float) m_dist(m_rand)};
		//    return true;
		//}
};

Catch::Generators::GeneratorWrapper<vec2d<float>> vector2_gen(float, float);
//	return Catch::Generators::GeneratorWrapper<vec2d<float>>(
//			std::unique_ptr<Catch::Generators::IGenerator<vec2d<float>>>(
//					new RandomVector2Generator(low, high)));
//}


/*
 * vector3 version
 */
class RandomVector3Generator : public Catch::Generators::IGenerator<vec3d<float>> {
	std::minstd_rand m_rand;
	std::uniform_real_distribution<> m_dist;
	vec3d<float> current_vector;
public:

	RandomVector3Generator(float low, float high):
		m_rand(),
		m_dist(low, high)
		{
			static_cast<void>(next());
		}

	//vec2d<float> const& get() const override;
	vec3d<float> const& get() const {
		return current_vector;
	}
	bool next() override {
		float d1 = m_dist(m_rand);
		float d2 = m_dist(m_rand);
		float d3 = m_dist(m_rand);
		current_vector = {d1, d2, d3};
		return true;
	}
};

Catch::Generators::GeneratorWrapper<vec3d<float>> vector3_gen(float, float);
//	return Catch::Generators::GeneratorWrapper<vec3d<float>>(
//			std::unique_ptr<Catch::Generators::IGenerator<vec3d<float>>>(
//					new RandomVector3Generator(low, high)));
//}
#endif // CATCH_VEC_GEN_HH
