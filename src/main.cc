#include <chrono>
#include <array>
#include <random>
#include <iostream>
#include "vector.hh"
#include "matrix.hh"
#include "quaternions.hh"

constexpr size_t size = 10'000;
constexpr size_t rotations = 3;


int main() {
	vector<float, 3>* points = new vector<float, 3>[size];
	vector<float, 3>* rotated_points = new vector<float, 3>[size];
	quaternion* rotated_quats = new quaternion[size];

	std::minstd_rand random_engine{};
	std::uniform_real_distribution<float> random_distribution{0.1, 0.9};

	for (size_t i = 0; i < size; ++i) {
		auto p = points[i];
		p[0] = random_distribution(random_engine);
		p[1] = random_distribution(random_engine);
		p[2] = random_distribution(random_engine);
	}

	matrix<float, 3> rot_mat1 = create_rotation_matrix(25, {0,0,1});
	matrix<float, 3> rot_mat2 = create_rotation_matrix(30, {0,1,0});
	matrix<float, 3> rot_mat3 = create_rotation_matrix(40, {1,0,0});
	auto rot_mat_123 = rot_mat3 * (rot_mat2 * rot_mat1);
	quaternion rot_qua1 = create_rotation(25, {0,0,1});
	quaternion rot_qua2 = create_rotation(30, {0,1,0});
	quaternion rot_qua3 = create_rotation(40, {1,0,0});
	auto rot_qua_123 = (rot_qua1 * rot_qua2) * rot_qua3;

	auto testpoint_mat = rot_mat_123 * points[0];
	auto testpoint_qua = rotate(rot_qua_123, points[0]);
	if (testpoint_mat != testpoint_qua) {
		std::cerr << "Matriisi ja kvaternio eivät vastaa:\nMatriisi:\n";
		print_matrix(rot_mat_123, std::cerr);
		std::cerr << "\nKvaternio:\n" << rot_qua_123 << "\nPiste: "
			<< points[0] << "\ntest_mat: " << testpoint_mat
			<< "\ntest_qua: " << testpoint_qua << '\n';
		std::exit(1);
	}
	


	std::array<quaternion, 3> quaternions{
			quaternion{0, 0, 0, 1},
			create_rotation(33, {1,0,0}),
			rot_qua_123
		};
	std::array<matrix<float, 3>, 3> matrices{
			matrix<float,3>{-1, 0, 0, 0, -1, 0, 0, 0, 1},
			matrix<float,3>{1, 0, 0, 0, 0.83867, -0.544639, 0, 0.544639, 0.83867},
			rot_mat_123
		};

	//for (int i = 0; i < 10; ++i) {
	//	std::cout << points[i] << " ja käännettynä:\n";
	//	std::cout << rotate(rotation, points[i]) << '\n';
	//}

	std::cout << size << " pisteen rotaatio kvaternioilla:\n";

	for (int round = 1; round <= 3; ++round) {
		std::cout << "Kierros " << round << ":\n";

		quaternion rotation = quaternions[round-1];
		matrix<float, 3> rot_matrix = matrices[round-1];


		// matriisimenetelmä
		auto start = std::chrono::steady_clock::now();
		for (size_t i = 0; i < size; ++i) {
			rotated_points[i] = rot_matrix * points[i];
		}
		auto end = std::chrono::steady_clock::now();
		std::chrono::duration<double> elapsed_mat = end-start;

		// hidas kvaterniomenetelmä
		start = std::chrono::steady_clock::now();
		for (size_t i = 0; i < size; ++i) {
			rotated_points[i] = rotate(rotation, points[i]);
		}
		end = std::chrono::steady_clock::now();
		std::chrono::duration<double> elapsed_quat = end-start;

		// nopeampi kvaterniomenetelmä
		quaternion rot_inverse = inverse(rotation);
		start = std::chrono::steady_clock::now();
		for (size_t i = 0; i < size; ++i) {
			rotated_quats[i] = (rotation*quaternion(points[i]))*rot_inverse;
		}
		end = std::chrono::steady_clock::now();
		std::chrono::duration<double> elapsed_quat_fast = end-start;
		// tarkistus
		for (size_t i = 0; i < size; ++i)
			if (rotated_quats[i] != rotated_points[i])
				std::cerr << "Problem, i: " << i << " points: " << rotated_points[i]
					<< " and " << rotated_quats[i] << '\n';

		// vielä nopeampi kvaterniomenetelmä
		start = std::chrono::steady_clock::now();
		for (size_t i = 0; i < size; ++i) {
			rotated_quats[i] = (rotation*points[i])*rot_inverse;
		}
		end = std::chrono::steady_clock::now();
		std::chrono::duration<double> elapsed_quat_faster = end-start;
		// tarkistus
		for (size_t i = 0; i < size; ++i)
			if (rotated_quats[i] != rotated_points[i])
				std::cerr << "Problem, i: " << i << " points: " << rotated_points[i]
					<< " and " << rotated_quats[i] << '\n';

		std::cout << "Kvaterniolaskuilla: " << elapsed_quat.count() << " s\n";
		std::cout << "kiinteällä q^-1   : " << elapsed_quat_fast.count() << " s\n";
		std::cout << "erikoistetulla *  : " << elapsed_quat_faster.count() << " s\n";
		std::cout << "Matriisilaskuilla : " << elapsed_mat.count() << " s\n";

	}
	delete[] rotated_quats;
	delete[] rotated_points;

	quaternion* quat_rots = new quaternion[rotations];
	matrix<float, 3>* mat_rots = new matrix<float, 3>[rotations];
	std::uniform_real_distribution<float> angle_distribution{-180.0, 180.0};

	for (size_t i = 0; i < rotations; ++i) {
		vector<float, 3> axis = {
			random_distribution(random_engine),
			random_distribution(random_engine),
			random_distribution(random_engine) };
		float theta = angle_distribution(random_engine);
		quat_rots[i] = create_rotation(theta, axis);
		mat_rots[i] = create_rotation_matrix(theta, axis);
	}

	auto start = std::chrono::steady_clock::now();
	quaternion qaccu = quat_rots[0];
	for (size_t i = 1; i < rotations; ++i) {
		qaccu = qaccu * quat_rots[i];
	}
	matrix<float, 3> final_mat_quats = create_matrix(qaccu);
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_quat_rots = end-start;


	start = std::chrono::steady_clock::now();
	matrix<float, 3> maccu = mat_rots[0];
	for (size_t i = 1; i < rotations; ++i) {
		maccu = maccu * mat_rots[i];
	}
	auto final_mat_mats = maccu;
	end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_mat_rots = end-start;

	if (final_mat_mats != final_mat_quats) {
		std::cout << "Test flawed:\nMatrix accumulation result:\n";
		print_matrix(final_mat_mats);
		std::cout << "Quaternion accumulation result:\n";
		print_matrix(final_mat_quats);
		std::cout << "with quaternion " << qaccu << '\n';
	}

	std::cout << "-------------------------------------------\n";
	std::cout << "Kvaternio- ja matriisikumulaation tulokset:\n";
	std::cout << "kvaterniokumulaatio : " << elapsed_quat_rots.count() << " s\n";
	std::cout << "matriisikumulaatio  : " << elapsed_mat_rots.count() << " s\n";


	delete[] points;
	delete[] quat_rots;
	delete[] mat_rots;

	return 0;
}
