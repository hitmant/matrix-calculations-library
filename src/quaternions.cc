#include "quaternions.hh"
#include <cmath>
constexpr float epsilon = 0.000001f;

std::ostream& operator<< (std::ostream& os, const quaternion& q){
	os << "<" << q.s << ", " << q.x << ", " << q.y << ", " << q.z << ">";
	return os;
}

bool operator== (const quaternion& lhs, const quaternion& rhs) {
	return (std::abs(lhs.s - rhs.s) < epsilon && 
	        std::abs(lhs.x - rhs.x) < epsilon && 
	        std::abs(lhs.y - rhs.y) < epsilon && 
	        std::abs(lhs.z - rhs.z) < epsilon );
}

quaternion operator+ (const quaternion& lhs, const quaternion& rhs) {
	return quaternion{lhs.s+rhs.s, lhs.x+rhs.x, lhs.y+rhs.y, lhs.z+rhs.z};
}

quaternion operator* (const quaternion& lhs, const quaternion& rhs) {
	return quaternion{
			(lhs.s*rhs.s) - (lhs.x*rhs.x) - (lhs.y*rhs.y) - (lhs.z*rhs.z),
			(lhs.s*rhs.x) + (lhs.x*rhs.s) + (lhs.y*rhs.z) - (lhs.z*rhs.y),
			(lhs.s*rhs.y) - (lhs.x*rhs.z) + (lhs.y*rhs.s) + (lhs.z*rhs.x),
			(lhs.s*rhs.z) + (lhs.x*rhs.y) - (lhs.y*rhs.x) + (lhs.z*rhs.s) };

}

// vec p = quat q with s=0
quaternion operator* (const quaternion& lhs, const vector<float, 3>& rhs) {
	return quaternion{
			- (lhs.x*rhs[0]) - (lhs.y*rhs[1]) - (lhs.z*rhs[2]),
			  (lhs.s*rhs[0]) + (lhs.y*rhs[2]) - (lhs.z*rhs[1]),
			  (lhs.s*rhs[1]) - (lhs.x*rhs[2]) + (lhs.z*rhs[0]),
			  (lhs.s*rhs[2]) + (lhs.x*rhs[1]) - (lhs.y*rhs[0]) };
}

//quaternion operator* (const vector<float, 3>& lhs, const quaternion& rhs) {
//	return quaternion{
//			(lhs.s*rhs.s) - (lhs.x*rhs.x) - (lhs.y*rhs.y) - (lhs.z*rhs.z),
//			(lhs.s*rhs.x) + (lhs.x*rhs.s) + (lhs.y*rhs.z) - (lhs.z*rhs.y),
//			(lhs.s*rhs.y) - (lhs.x*rhs.z) + (lhs.y*rhs.s) + (lhs.z*rhs.x),
//			(lhs.s*rhs.z) + (lhs.x*rhs.y) - (lhs.y*rhs.x) + (lhs.z*rhs.s) };
//
//}


quaternion operator/ (const quaternion& q, float d) {
	return quaternion{q.s/d, q.x/d, q.y/d, q.z/d};
}

float magn (const quaternion& q) {
	return sqrt(q.s*q.s + q.x*q.x + q.y*q.y + q.z*q.z);
}

quaternion conjugate(const quaternion& q) {
	return quaternion{q.s, -q.x, -q.y, -q.z};
}

quaternion inverse(const quaternion& q) {
	quaternion inv = conjugate(q);
	inv = inv / (magn(q) * magn(q));
	return inv;
}

vector<float, 3> rotate(const quaternion& q, const vector<float, 3>& p) {
	quaternion pq{p};
	pq = (q*pq)*inverse(q);
	//if (pq.s > 0.0001f || pq.s < -0.0001f)
	//	std::cerr << "Rotation error: " << pq << " with q: " << q 
	//		<< " and p: " << p << '\n';
	return vector<float, 3>{pq.x, pq.y, pq.z};
}

quaternion create_rotation(float theta, const vector<float, 3>& axis) {
	theta = (theta*2*3.1415926)/360;
	auto sine = std::sin(theta/2);
	auto scaled_axis = sine * make_unit(axis);
	return quaternion{std::cos(theta/2), scaled_axis[0], scaled_axis[1], scaled_axis[2]};
}

matrix<float, 3> create_matrix(const quaternion& q) {
	return matrix<float, 3>{
		1-2*q.y*q.y-2*q.z*q.z, 2*q.x*q.y+2*q.s*q.z,   2*q.x*q.z-2*q.s*q.y,
		2*q.x*q.y-2*q.s*q.z,   1-2*q.x*q.x-2*q.z*q.z, 2*q.y*q.z+2*q.s*q.x,
		2*q.x*q.z+2*q.s*q.y,   2*q.y*q.z-2*q.s*q.x,   1-2*q.x*q.x-2*q.y*q.y
	};
}
