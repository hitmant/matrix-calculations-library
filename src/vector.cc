#include "vector.hh"

/*
template <typename T>
auto cross(const T& l, const T& r) {
	if constexpr (std::is_same<T, vec3d<double>>::value) {
			return vec3d{
					l.y*r.z - r.y*l.z,
					l.z*r.x - r.z*l.x,
					l.x*r.y - r.x*l.y};
		}
	else if constexpr (std::is_same<T, vec2d<double>>::value) {
			return l.x*r.y - r.x*l.y;
		}
}
*/

//template <Number N>
//N cross(const vec2d<N>& l, const vec2d<N>& r) {
//	return l.x*r.y - r.x*l.y;
//}

//template <Number N>
//vec3d<N> cross(const vec3d<N>& l, const vec3d<N>& r) {
//	return vec3d{
//			l.y*r.z - r.y*l.z,
//			l.z*r.x - r.z*l.x,
//			l.x*r.y - r.x*l.y};
//}
