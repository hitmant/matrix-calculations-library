#include "../include/catch.hpp"
#include "vector.hh"
#include "matrix.hh"
//#include "catch_vec_generator.hh"


SCENARIO("matrix calculations", "[matrix]") {
	GIVEN("a matrix and a scalar") {
		//mat2  matrix = GENERATE(take(10, matrix2_gen(-5.0, 5.0)));
		float scalar = GENERATE(take(5, random(-10, 10)));
		mat2<float> matrix = {1.0, 0.5, 2.5, .765};

		WHEN("the matrix is multiplied with the scalar") {
			auto res = scalar * matrix;

			THEN("the result a componentwise multiplication") {
				REQUIRE(res == mat2<float>{matrix(0,0)*scalar, matrix(1,0)*scalar,
				              matrix(0,1)*scalar, matrix(1,1)*scalar });
			}
			AND_THEN("the multiplication is commutative") {
				REQUIRE(res == matrix * scalar);
			}
		}
	}
	GIVEN("a matrix and a vector") {
		mat2<float> matrix = {1.0, 0.5, 2.5, .765};
		vec2d<float> vector = {0.136, -.542};

		WHEN("they are multiplied") {
			auto res = matrix * vector;
			vec2d<float> correct = {-1.219, -0.34663};
			bool comparison = (res == correct);
			REQUIRE(comparison);

			THEN("the result is a transformed matrix") {
				REQUIRE(res == correct);
			}
		}
	}
	GIVEN("four dimensional transformation matrix and a homog vector") {
		float d1 = GENERATE(take(4, random(-1.0, 1.0)));
		float d2 = GENERATE(take(2, random(-1.0, 1.0)));
		float d3 = GENERATE(take(1, random(-1.0, 1.0)));
		float d4 = GENERATE(take(1, random(-1.0, 1.0)));
		mat4<float> trans = {d1, d2, d3, 0, d4, d1, d2, 0,
		                      d3, d4, d1, 0, 1.0, 2.0, 3.0, 1.0};
		vec4d<float> posit = {d4, d2, d1, 1.0};

		WHEN("the vector is multiplied by the matrix from the left") {
			vec4d<float> res = trans * posit;
			THEN("the result is an oriented and translated vector") {
				REQUIRE(std::fabs(res.w - 1.0f) < tolerance<float>);
			}
		}
	}
	GIVEN("a four dimensional matrix and vector") {
		vec4d<float> vec = {-0.290222, 0.0976879, -0.29213, 1};
		mat4<float>  mat = {-0.29213, 0.0976879, 0.27542, 0,
		                    -0.290222, -0.29213, 0.0976879, 0,
		                    0.27542, -0.290222, -0.29213, 0,
		                    1, 2, 3, 1};
		vec4d<float> cor = {0.013867, 0.0271537, -0.0229442, 0.0287638};

		WHEN("the matrix is multiplied by the vector from the left") {
			THEN("the result is a vector") {
				REQUIRE(vec*mat == cor);
			}
		}
	}
	GIVEN("two matrices") {
		mat4<float> mat1 = {-0.29213, 0.0976879, 0.27542, 5,
		                    -0.290222, -0.29213, 4, 0,
		                    0.27542, 3, -0.29213, 0,
		                    2, 2, 3, 1};
		mat4<float> mat2 = {2, 0.0976879, 0.27542, 0,
		                    -0.290222, 3, 0.0976879, 0,
		                    0.27542, -0.290222, 4, 0,
		                    1, 2, 3, 5};
		mat4<float> corr = {-0.536755,  0.993098,  0.861133, 10,
		                    -0.758978, -0.611677, 11.8915,   -1.45111,
		                    1.10545,   12.1117,   -2.25355,   1.3771,
		                    9.95369,   18.5134,   22.399,    10 };
		WHEN("they are multiplied together") {
			mat4<float> res = mat1 * mat2;
			THEN("the result is a multiplied matrix") {
				REQUIRE(res == corr);
			}
		}


}
}

TEST_CASE( "rotation matrix creation", "[rmat]") {
	vector<float, 3> axis = {1,0,0};
	matrix<float, 3> rot = create_rotation_matrix(90, axis);

	CHECK( std::abs(rot(0, 0) - 1.0f) < 0.0001f );
	CHECK( std::abs(rot(1, 1) - 0.0f) < 0.0001f );
	CHECK( std::abs(rot(1, 2) + 1.0f) < 0.0001f );
	CHECK( std::abs(rot(2, 1) - 1.0f) < 0.0001f );
	print_matrix(rot);
	//CHECK( rot[1][1] == 0 );
	//CHECK( rot[1][2] == -1 );
	//CHECK( rot[2][1] == 1 );
}
//{{-0.29213 , -0.290222 ,  0.27542 , 1},
// {0.0976879, -0.29213  , -0.290222, 2},
// {0.27542  ,  0.0976879, -0.29213 , 3},
// {0        ,  0        ,  0       , 1}}

//{{-0.536755, -0.758978,  1.10545,  9.95369},
// {0.993098,  -0.611677, 12.1117,  18.5134},
// {0.861133,  11.8915,   -2.25355, 22.399},
// {10,        -1.45111,   1.3771,  10}}

