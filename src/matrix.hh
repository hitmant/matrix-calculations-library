#ifndef MATRIX_HH
#define MATRIX_HH

#include <iostream>
#include <concepts>

#include "vector.hh"

/*  Column major matrix. */
template <Number N, int D>
struct matrix {
	N data[D*D];

	N& operator() (size_t row, size_t col) {
		return data[col*D + row];
	}
	const N& operator() (size_t row, size_t col) const {
		return data[col*D + row];
	}
};

template <Number N>
using mat2 = matrix<N, 2>;
template <Number N>
using mat3 = matrix<N, 3>;
template <Number N>
using mat4 = matrix<N, 4>;

template <Number N, int D>
void print_matrix(const matrix<N, D>& mat, std::ostream& os = std::cout) {
	auto oldwidth = os.width();
	os.width(8);
	os << "\n{";
	for (int row = 0; row < D; ++row) {
		os << (row == 0 ? " " : "   ");
		for (int col = 0; col < D-1; ++col) {
			os << mat(row, col) << ", ";
		}
		os << mat(row, D-1) << '\n';
	}
	os << "}\n";
	os.width(oldwidth);
}

template <Number N, Number S, int D>
matrix<N, D> operator* (const S& scal, const matrix<N, D>& mat) {
	matrix<N, D> result{};
	for (size_t i = 0; i < D*D; ++i) {
		result.data[i] = mat.data[i] * scal;
	}
	return result;
}

template <Number N, Number S, int D>
matrix<N, D> operator* (const matrix<N, D>& mat, const S& scal) {
	return scal*mat;
}

template <Number N, int D>
bool operator== (const matrix<N, D>& lhs, const matrix<N, D>& rhs) {
	for (int row = 0; row < D; ++row)
		for (int col = 0; col < D; ++col) {
			if (std::fabs(lhs(row, col) - rhs(row, col)) > tolerance<N>)
				return false;
		}
	return true;
}

template <Number N, int D>
vector<N, D> operator* (const matrix<N, D>& mat, const vector<N, D> vec) {
	vector<N, D> res{};
	for (int row = 0; row < D; ++row)
		for (int col = 0; col < D; ++col) {
			res[row] += vec[col]*mat(row, col);
		}
	return res;
}

template <Number N, int D>
vector<N, D> operator* (const vector<N, D> vec, const matrix<N, D>& mat) {
	vector<N, D> res{};
	for (int col = 0; col < D; ++col)
		for (int row = 0; row < D; ++row)
			res[col] += mat(row, col) * vec[row];
	return res;
}

template <Number N, int D>
matrix<N, D> operator* (const matrix<N, D>& left, const matrix<N, D>& right) {
	matrix<N, D> res{};
	for (int row = 0; row < D; ++row)
		for (int col = 0; col < D; ++col)
			for (int it = 0; it < D; ++it)
				res(row, col) += left(row, it) * right(it, col);
	return res;
}

matrix<float, 3> create_rotation_matrix(float, const vector<float, 3>&);

#endif

//template <Number N, int D, template<typename> typename V>
//V<N> operator* (const matrix<N, D>& mat, const V<N>& vec) {
//	/****/ if constexpr (D == 2 && std::is_same<V<N>, vec2d<N>>::value) {
//		return vec2d{
//			vec.x*mat(0,0) + vec.y*mat(0,1),
//			vec.x*mat(1,1) + vec.y*mat(1,1)
//			};
//	} else if constexpr (D == 3 && std::is_same<V<N>, vec3d<N>>::value) {
//		return vec2d{
//			vec.x*mat(0,0) + vec.y*mat(0,1) + vec.z*mat(0,2),
//			vec.x*mat(1,0) + vec.y*mat(1,1) + vec.z*mat(1,2),
//			vec.x*mat(2,0) + vec.y*mat(2,1) + vec.z*mat(2,2)
//			};
//	}
//}
