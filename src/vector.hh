#ifndef MATVEC_VECTOR_HH
#define MATVEC_VECTOR_HH
#include <iostream>
#include <concepts>
#include <vector>
#include <array>
#include <cmath>

template <typename T>
concept Number = std::integral<T> || std::floating_point<T>;

template <Number N>
constexpr N tolerance = std::numeric_limits<N>::epsilon() * 10000;

template <Number N, int D>
struct vector;

template <Number N>
struct vec2d : vector<N, 2> {
	N x;
	N y;

	vec2d<N>() = default;
	vec2d<N>(const vec2d<N>&) = default;
	vec2d<N>(const std::initializer_list<N>& ini) :
		vector<N, 2>(ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
	}
	vec2d<N>(const vector<N, 2>& v) :
		vector<N, 2>(v), x(v.data[0]), y(v.data[1]) {}
	constexpr vec2d<N>& operator=(const vec2d<N>&) = default;
	constexpr vec2d<N>& operator=(const std::initializer_list<N>& ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
		vector<N, 2>::data[0] = x;
		vector<N, 2>::data[1] = y;
		return *this;
	}
};

template <Number N>
struct vec3d : vector<N, 3>{
	N x;
	N y;
	N z;

	vec3d<N>() = default;
	vec3d<N>(const vec3d<N>&) = default;
	vec3d<N>(const std::initializer_list<N>& ini) :
		vector<N, 3>(ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
		z = siz >= 3 ? *it++ : 0;
	}
	vec3d<N>(const vector<N, 3>& v) :
		vector<N, 3>(v), x(v.data[0]),
		y(v.data[1]), z(v.data[2]) {}
	constexpr vec3d<N>& operator=(const vec3d<N>&) = default;
	constexpr vec3d<N>& operator=(const std::initializer_list<N>& ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
		z = siz >= 3 ? *it++ : 0;
		vector<N, 3>::data[0] = x;
		vector<N, 3>::data[1] = z;
		vector<N, 3>::data[2] = y;
		return *this;
	}
};

template <Number N>
struct vec4d : vector<N, 4>{
	N x;
	N y;
	N z;
	N w;

	vec4d<N>() = default;
	vec4d<N>(const vec4d<N>&) = default;
	vec4d<N>(const std::initializer_list<N>& ini) :
		vector<N, 4>(ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
		z = siz >= 3 ? *it++ : 0;
		w = siz >= 4 ? *it++ : 0;
	}
	vec4d<N>(const vector<N, 4>& v) :
		vector<N, 4>(v), x(v.data[0]),
		y(v.data[1]), z(v.data[2]), w(v.data[3]) {}
	constexpr vec4d<N>& operator=(const vec4d<N>&) = default;
	constexpr vec4d<N>& operator=(const std::initializer_list<N>& ini) {
		size_t siz = ini.size();
		auto it = std::begin(ini);
		x = siz >= 1 ? *it++ : 0;
		y = siz >= 2 ? *it++ : 0;
		z = siz >= 3 ? *it++ : 0;
		w = siz >= 4 ? *it++ : 0;
		vector<N, 4>::data[0] = x;
		vector<N, 4>::data[1] = z;
		vector<N, 4>::data[2] = y;
		vector<N, 4>::data[3] = w;
		return *this;
	}
};

template <Number N, int D>
struct vector {
	N data[D];

	vector<N, D>() = default;
	vector<N, D>(const vec2d<N>& vec) :
		data()
		{data[0] = vec.x; data[1] = vec.y;}
	vector<N, D>(const vec3d<N>& vec) :
		data()
		{data[0] = vec.x; data[1] = vec.y; data[2] = vec.z;}
	vector<N, D>(const vec4d<N>& vec) :
		data()
		{data[0] = vec.x; data[1] = vec.y;
		 data[2] = vec.z; data[3] = vec.w;}
	vector<N, D>(const std::initializer_list<N> il) {
		int siz = il.size();
		auto it = std::begin(il);
		for (int i = 0; i < std::min(D, siz); ++i)
			data[i] = *it++;
	}

	N& operator[] (size_t i)
		{ return data[i]; }
	const N& operator[] (size_t i) const
		{ return data[i]; }
};

template <Number N>
struct color : vec3d<N> {
	N& r = this->x;
	N& g = this->y;
	N& b = this->z;
};

template <Number N, int D>
std::ostream& operator<< (std::ostream& os, const vector<N, D> vec) {
	os << '{';
	for (int i = 0; i < D-1; ++i)
		os << vec[i] << ", ";
	os << vec[D-1] << '}';
	return os;
}

template <Number N, int D>
bool operator== (const vector<N, D> lhs, const vector<N, D> rhs) {
	for (int i = 0; i < D; ++i) {
		if (fabs(lhs.data[i] - rhs.data[i]) > tolerance<N>)
			return false;
	}
	return true;
}

template <Number N, int D>
vector<N, D> operator- (const vector<N, D> vec) {
	vector<N, D> res{};
	for (int i = 0; i < D; ++i)
		res.data[i] = -vec.data[i];
	return res;
}

template <Number N, int D>
vector<N, D> operator+ (const vector<N, D> lhs, const vector<N, D> rhs) {
	vector<N, D> res{};
	for (int i = 0; i < D; ++i)
		res.data[i] = lhs.data[i] + rhs.data[i];
	return res;
}

template <Number N, int D>
vector<N, D> operator- (const vector<N, D> lhs, const vector<N, D> rhs) {
	vector<N, D> res{};
	for (int i = 0; i < D; ++i)
		res.data[i] = lhs.data[i] - rhs.data[i];
	return res;
}

template <Number N, int D>
N operator* (const vector<N, D> lhs, const vector<N, D> rhs) {
	N res = 0;
	for (int i = 0; i < D; ++i)
		res += lhs.data[i] * rhs.data[i];
	return res;
}

template <Number N, int D, Number S>
vector<N, D> operator* (S scalar, const vector<N, D> rhs) {
	vector<N, D> res{};
	for (int i = 0; i < D; ++i)
		res.data[i] = scalar * rhs.data[i];
	return res;
}

template <Number N, int D, Number S>
vector<N, D> operator* (const vector<N, D> vector, S scalar) {
	return scalar * vector;
}

template <Number N, template<Number> typename T>
auto cross(const T<N>& l, const T<N>& r) {
	if constexpr (std::is_same<T<N>, vec4d<N>>::value) {
			return vec4d{
				l.y*r.z - r.y*l.z,
				l.z*r.x - r.z*l.x,
				l.x*r.y - r.x*l.y,
				static_cast<N>(1) };
		}
	else if constexpr (std::is_same<T<N>, vec3d<N>>::value) {
			return vec3d{
				l.y*r.z - r.y*l.z,
				l.z*r.x - r.z*l.x,
				l.x*r.y - r.x*l.y };
		}
	else if constexpr (std::is_same<T<N>, vec2d<N>>::value) {
			return l.x*r.y - r.x*l.y;
		}
}

template <Number N, int D>
vector<N, D> make_unit(const vector<N, D>& orig) {
	N magnitude = std::sqrt(orig * orig);
	vector<N, D> res{};
	for (int i = 0; i < D; ++i) {
		res[i] = orig[i] / magnitude;
	}
	return res;
}

#endif // MATVEC_VECTOR_HH
