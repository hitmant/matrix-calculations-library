#include "../include/catch.hpp"
#include "vector.hh"
#include "catch_vec_generator.hh"

SCENARIO( "Vectors can be used in arithmetic operations.", "[vect]" ) {

	GIVEN("two vec2ds with the same members") {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		vec2d v1 = {d1, d2};
		vec2d v2 = {d1, d2};

			WHEN( "they are compared" ) {
					THEN( "they compare equal" ) {
						REQUIRE( v1 == v2 );
					}
					THEN("the comparison is commutative") {
						REQUIRE(v2 == v1);
					}
			}

	}

	GIVEN( "Two vectors with initial values") {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		vec2d v1 = {d1, d2};
		vec2d v2 = {d2, d1};

		const auto v1_orig = v1;
		const auto v2_orig = v2;

		REQUIRE(v1 == v1_orig);
		REQUIRE(v2 == v2_orig);

		WHEN( "they are summed" ) {
			vec2d res = v1 + v2;

			THEN( "the result is a memberwise sum" ) {
				REQUIRE( res.x == v1.x + v2.x );
				REQUIRE( res.y == v1.y + v2.y );
				REQUIRE(v1 == v1_orig);
				REQUIRE(v2 == v2_orig);
				assert( res.x == v1.x + v2.x );
			}
		}

		WHEN( "they are subracted" ) {
			vec2d res = v1 - v2;

			THEN( "the result is a memberwise subtraction" ) {
				REQUIRE( res.x == v1.x - v2.x );
				REQUIRE( res.y == v1.y - v2.y );
				REQUIRE(v1 == v1_orig);
				REQUIRE(v2 == v2_orig);
			}
		}

		WHEN( "their dot product is calculated" ) {
			double res = v1 * v2;

			THEN( "the result is a scalar" ) {
				REQUIRE( res == v1.x*v2.x + v1.y*v2.y );
				REQUIRE(v1 == v1_orig);
				REQUIRE(v2 == v2_orig);
			}
		}

		WHEN( "their cross product is calculated " ) {
				double res = cross(v1, v2);

			THEN( "the result is a scalar" ) {
					REQUIRE(res == v1.x*v2.y - v2.x*v1.y);
					REQUIRE(v1 == v1_orig);
					REQUIRE(v2 == v2_orig);
			}
		}
	}

	GIVEN( "two three dimensional vectors with initial values " ) {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		auto d3 = GENERATE(take(1, random(-5.0, 5.0)));
		vec3d v1 = {d1, d2, d3};
		vec3d v2 = {d2, d3, d1};

		WHEN( "they are summed" ) {
			vec3d res = v1 + v2;

			THEN( "the result is a memberwise sum" ) {
				REQUIRE(res == vec3d{v1.x + v2.x, v1.y + v2.y, v1.z + v2.z});
				REQUIRE(res.x == v1.x + v2.x);
				REQUIRE(res.y == v1.y + v2.y);
				REQUIRE(res.z == v1.z + v2.z);
			}
		}

		WHEN("they are subtracted") {
			vec3d res = v1 - v2;

			THEN("the result is a memberwise subtraction") {
				REQUIRE(res.x == v1.x - v2.x);
				REQUIRE(res.y == v1.y - v2.y);
				REQUIRE(res.z == v1.z - v2.z);
			}
		}

		WHEN("their dot product is calculated") {
				auto res = v1 * v2;

			THEN("the result is a scalar") {
				REQUIRE(res == v1.x*v2.x + v1.y*v2.y + v1.z*v2.z);
			}
			THEN("and the dot product is commutative") {
				REQUIRE(res == v2 * v1);
			}
		}
	}

	GIVEN("a vector and a scalar") {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		vec2d vec = {d1, d2};
		float scalar = GENERATE(take(5, random(-10.0, 10.0)));

		WHEN("the scalar multiplies a vector") {
			auto res = scalar * vec;
			THEN("the result is a scaled vector") {
				REQUIRE(res == vec2d{vec.x*scalar, vec.y*scalar});
			}
			AND_THEN("the result is commutative") {
				REQUIRE(res == vec*scalar);
			}
		}
	}

	GIVEN("two four dimensional vectors") {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		auto d3 = GENERATE(take(1, random(-5.0, 5.0)));
		auto d4 = GENERATE(take(1, random(-5.0, 5.0)));
		vec4d v1 = {d1, d2, d3, d4};
		vec4d v2 = {d2, d3, d4, d1};

		WHEN("they are summed") {
			vec4d res = v1 + v2;
			THEN("the result is a componentwise summing") {
				REQUIRE(res.x == v1.x + v2.x);
				REQUIRE(res.y == v1.y + v2.y);
				REQUIRE(res.z == v1.z + v2.z);
				REQUIRE(res.w == v1.w + v2.w);
			}
			AND_THEN("the operation is commutative") {
				REQUIRE(res == v2 + v1);
			}
		}
		WHEN("they are subtracted") {
			vec4d res = v1 - v2;
			THEN("the result is a componentwise subtraction") {
				REQUIRE(res.x == v1.x - v2.x);
				REQUIRE(res.y == v1.y - v2.y);
				REQUIRE(res.z == v1.z - v2.z);
				REQUIRE(res.w == v1.w - v2.w);
			}
		}
		WHEN("their dot product is calculated") {
			auto res = v1 * v2;
			THEN("the result is a scalar") {
				REQUIRE(res == v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w);
			}
			THEN("the operation is commutative") {
				REQUIRE(res == v2 * v1);
			}
		}
		WHEN("their cross product is calculated") {
			v1.w = 1.0f; //the vector cross product is not defined for 4d
			v2.w = 1.0f; //vectors, so handle them as homogenous 3d vecs.
			vec4d res = cross(v1, v2);
			vec4d opp = cross(v2, v1);
			opp.w = -1;
			THEN("the result is an orthogonal vector") {
				REQUIRE(std::fabs(res * v1 - 1.0f) < tolerance<float>);
				REQUIRE(std::fabs(res * v2 - 1.0f) < tolerance<float>);
			}
			AND_THEN("WHEN the operands are flipped, the result is an opposite vector") {
				REQUIRE(res == -opp);
			}
		}
	}
	GIVEN("a 4D-vector and a scalar") {
		auto d1 = GENERATE(take(2, random(-5.0, 5.0)));
		auto d2 = GENERATE(take(1, random(-5.0, 5.0)));
		auto d3 = GENERATE(take(1, random(-5.0, 5.0)));
		auto d4 = GENERATE(take(1, random(-5.0, 5.0)));
		auto scalar = GENERATE(take(2, random(-50.0, 50.0)));
		vec4d v1 = {d1, d2, d3, d4};

		WHEN("they are multiplied together") {
			vec4d res = scalar * v1;
			vec4d com = v1 * scalar;
			THEN("the result is a scaled vector") {
				REQUIRE(res.x == v1.x*scalar);
				REQUIRE(res.y == v1.y*scalar);
				REQUIRE(res.z == v1.z*scalar);
				REQUIRE(res.w == v1.w*scalar);
			}
			THEN("the operation is commutative") {
				REQUIRE(res == com);
			}
		}
	}

}

TEST_CASE("vector normalisation", "[vecn]") {
	vector<float, 3> v1{1,1,0};
	CHECK( make_unit(v1) == vector<float, 3>{0.707107, 0.707107, 0} );
}

