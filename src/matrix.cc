#include "matrix.hh"

auto sine = [](float theta){
	return std::sin((theta*2*3.1415926)/360);
};
auto cosin = [](float theta){
	return std::cos((theta*2*3.1415926)/360);
};

matrix<float, 3> create_rotation_matrix(float t, const vector<float, 3>& A) {
	float s = sine(t);
	float c = cosin(t);
	auto a = make_unit(A);
	float x = a[0];
	float y = a[1];
	float z = a[2];
	return matrix<float, 3>{
		c+(1-c)*x*x,   (1-c)*x*y+s*z, (1-c)*x*z-s*y,
		(1-c)*x*y-s*z, c+(1-c)*y*y,   (1-c)*y*z+s*x,
		(1-c)*x*z+s*y, (1-c)*y*z-s*x, c+(1-c)*z*z
	};
}
