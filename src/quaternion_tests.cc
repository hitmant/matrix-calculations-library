#include "../include/catch.hpp"
#include "quaternions.hh"

TEST_CASE("quaternion calculations", "[quat]") {
	quaternion q1{2,-2,3,-4};
	quaternion q2{1,-2,5,-6};
	
	REQUIRE( q1+q2 == quaternion{3,-4,8,-10} );
	REQUIRE( q1*q2 == quaternion{-41, -4, 9, -20} );
	REQUIRE( q1*q1 == quaternion{-25, -8, 12, -16} );
	CHECK( magn(q1) < 5.74457f );
	CHECK( magn(q1) > 5.74455f );
	CHECK( inverse(q1) == quaternion{0.060606060, 0.06060606, -0.09090909, 0.1212121212} );
}

TEST_CASE("quaternion rotations", "[qrot]") {
	quaternion q1{0.707107, 0.707107, 0, 0};
	quaternion q2{0.707107, -0.707107, 0, 0};
	quaternion r = create_rotation(90, {1,0,0});
	quaternion r1 = create_rotation(90, {6,6,0});
	quaternion p{0, 0, 1, 0};
	vector<float, 3> pv{p.x, p.y, p.z};
	matrix<float, 3> rmat = {1, 0, 0, 0, 0, 1, 0, -1, 0};
	matrix<float, 3> qmat = create_matrix(q1);

	CHECK( r == q1 );
	CHECK( (q1*p)*q2 == quaternion{0,0,0,1} );
	CHECK( rotate(q1, pv) == vector<float, 3>{0,0,1} );
	CHECK( rotate(r, pv)  == vector<float, 3>{0,0,1} );
	CHECK( magn(r1) > 0.999f );
	CHECK( magn(r1) < 1.001f );
	CHECK( rotate(r1, pv) == vector<float, 3>{0.5,0.5,0.707107} );
	CHECK( rmat == qmat );
}
