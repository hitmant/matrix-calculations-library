#ifndef QUATERNIONS_HH
#define QUATERNIONS_HH

#include <iostream>
#include <concepts>
#include "vector.hh"
#include "matrix.hh"

struct quaternion {
	float s;
	float x;
	float y;
	float z;

	quaternion() = default;
	quaternion(const vector<float,3>& vec) :
		s(0.0f), x(vec[0]), y(vec[1]), z(vec[2]) {};
	quaternion(float is, float ix, float iy, float iz) :
		s(is), x(ix), y(iy), z(iz) {};
};

bool operator== (const quaternion&, const quaternion&);

std::ostream& operator<< (std::ostream&, const quaternion&);

quaternion operator+ (const quaternion&, const quaternion&);

quaternion operator* (const quaternion&, const quaternion&);

quaternion operator/ (const quaternion&, float);

float magn(const quaternion&);

quaternion conjugate(const quaternion&);

quaternion inverse(const quaternion&);

vector<float, 3> rotate(const quaternion&, const vector<float, 3>&);

quaternion create_rotation(float, const vector<float, 3>& = {1,0,0});

matrix<float, 3> create_matrix(const quaternion&);

#endif
